close all
clear
inimg = imread('../data/mirrorFlat/Flat0000.bmp');


% pt1 = [  580,  556,  952,  965;
%          1396, 1644, 1644, 1397;
%          1,    1,    1,    1;];
% 
% pt2 = [ 20, 20, 70, 70;
%         20, 70, 70, 20;
%         1,  1,  1,  1;];
    
pt1 = [  580,  965,  952,  556;
        1396, 1397, 1644, 1644;
           1,    1,    1,    1;];
    
ptRef = [ 20, 70, 70, 20;
          20, 20, 70, 70;
           1,  1,  1,  1;];
ptRef = [ 0, 50, 50,  0;
          0,  0, 50, 50;
          1,  1,  1,  1;];    
    
Hsc = homography2d(pt1, ptRef);
Hsc2 = Hsc./Hsc(3,3);
Hsc2(3,1:2) = 0;
[outimg1,newT] = imTrans(inimg,Hsc2);
pt2 = Hsc*pt1;
pt2 = pt2./pt2(3,:);

pt3 = Hsc2*pt1;
pt3 = pt3./pt3(3,:);

tform = affine2d(Hsc2');
outimg2 = imwarp(inimg,tform);

% display the result
subplot(1,3,1);
imshow(inimg);
hold on
plot(pt1(1,:),pt1(2,:),'*')
subplot(1,3,2);
imshow(outimg1,[0 255]);
subplot(1,3,3);
imshow(outimg2,[0 255]);
imgname = 'm7';
IpImage = imread(['../mirror image acquisition/cropped/' imgname '.JPG']); 
GrImage = rgb2gray(IpImage); GrImage = imadjust(GrImage);
[x y] = size(GrImage);
GrImage = imresize(GrImage,[x/4 y/4])
figure(1), imshow(GrImage)

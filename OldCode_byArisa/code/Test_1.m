IpImage = imread('../mirror data/MIRROR-0012.JPG');
GrImage = rgb2gray(IpImage); figure(1), imshow(GrImage)
BWImage_1 = imbinarize(GrImage,'global');
FillImg = imfill(BWImage_1,'holes');

CompImg = imcomplement(FillImg);
se = strel('disk',3);
openImg = imopen(CompImg,se); 
FillopImg = imfill(openImg,'holes');
CompImg = imcomplement(FillopImg);figure(2), imshow(CompImg)

%%remove background 
GrImage2 = GrImage;

%---------------------------------------------------
% procetive_geometry.m
% Kurt Niel, Gernot Stuebl, AT
% using /MatlabFns/Projective
%   as part of the huge matlab library for machine vision
%   by Peter Kovesi, AU
% Nov 2017
%
% projective geometry by homography matrix
% modify parameters of the homography matrix
%   for getting a feeling of their impact to the translation
%---------------------------------------------------

% preamble
close all;
clear all;
clc;
addpath(genpath('Images'));
addpath(genpath('MatlabFns/Projective'));

% selecting input image by uncomment
%inimg = imread('chessboard.png');
% inimg = imread('kepleruhrS.JPG');
%inimg = imread('duden.JPG');
inimg = imread('../mirror image acquisition/chess.bmp');
inimg = rgb2gray(inimg);
[w h]=size(inimg);
inimg = imresize(inimg,[2*195 2*259]);
figure(1),imshow(inimg)

% mask = roipoly;
% for m = 1:size(inimg,1)
%     for n = 1:size(inimg,2)
%         if mask(m,n) == 0
%             inimg(m,n)= 0;
%         end
%     end
% end
% figure(1),imshow(inimg)

% standard affine translation
scalex = 1; % scale factor
scaley = 1; % scale factor
shearx = 0;
sheary = 0;
angle = 0/180*pi; % rotation angle
tx =    0; % x translation
ty =    0; % y translation
bregion = [51 150 51 150];

% affine translation - rotation x scale x shear x translation
h11 = cos(angle)*scalex-sin(angle)*scaley*sheary;
h12 = -cos(angle)*scalex*shearx-sin(angle)*scaley;
h13 = cos(angle)*tx-sin(angle)*ty;
h21 = sin(angle)*scalex+cos(angle)*scaley*sheary;
h22 = sin(angle)*scalex*shearx+cos(angle)*scaley;
%h22 = scaley;
h23 = cos(angle)*ty+sin(angle)*tx;
h31 = 0;
h32 = 0;
h33 = 1;
Hsa = [h11 h12 h13;
       h21 h22 h23;
       h31 h32 h33;];
 
% general projective homography by empiric approach
Hsp = [ 1.0000  0.0001  0.0000;
        0.0001  1.0000  0.0000;
        0.0000  0.0000  1;];
% projective homography kepleruhr.JPG
Hspk = [ 3.0000 -0.2500  0.0000;
         1.0500  1.0000  0.0000;
         0.0045 -0.0005  1;];
% projective homography duden.JPG
Hspd = [ 0.0400  0.8800  0.0000;
        -0.4500  0.0800  0.0000;
        -0.0010 -0.0005  1;];

% calculating homography H by corresponding points x1, x2
% H = homography(x1, x2)
%       x1: 3xN set of homogeneous points
%       x2: 3xN set of homogeneous points such that x1<->x2
%       H:  the 3x3 homography such that x2 = H*x1
%
% x1: for checking the operation: here x1 = x2
x1 = [ 20 450 450  20;
       20  20 320 320;
        1   1   1   1;];
% x1: actual points within the image/camera plane
%x1 = [261 269  86  52;
%       72 193 258 103;
%        1   1   1   1;];
[xp1 xp2]=getpts;

x1 = [xp1(1) xp1(2) xp1(3) xp1(4);
      xp2(1) xp2(2) xp2(3) xp2(4);
        1   1   1   1;];

% x2: desired points for the output plane
x2 = [ 20 450 450  20;
       20  20 320 320;
        1   1   1   1;];

Hsc = homography2d(x1, x2);

% selecting operation by uncomment
%outimg = imTrans(inimg,Hsa);
%outimg = imTrans(inimg,Hsp);
%outimg = imTrans(inimg,Hspk);
%outimg = imTrans(inimg,Hspd);
outimg = imTrans(inimg,Hsc);
outimg=imcrop(outimg);
outimg = imresize(outimg,[2*195 2*259]);
%outimg = imTrans(inimg,Hsp,bregion);

% display the result
figure(1)
% subplot(2,1,1); 
imshow(inimg);hold on
plot([xp1(1),xp1(2)],[xp2(1),xp2(2)],'g')
plot([xp1(3),xp1(2)],[xp2(3),xp2(2)],'g')
plot([xp1(1),xp1(4)],[xp2(1),xp2(4)],'g')
plot([xp1(4),xp1(3)],[xp2(4),xp2(3)],'g')

figure(2)
% subplot(2,1,2);
imshow(outimg,[0 255]); 

% 
% % %reflection
% %reflect about y-axis
x3 = [-1 0 0;
       0 1 0 ;
       0 0 1 ;];
% %%reflect about x-axis
% % x3 = [1 0 0;
% %        0 -1 0 ;
% %        0 0 1 ;];   
% 
% %%reflect about origin
% % x3 = [-1 0 0;
% %        0 -1 0 ;
% %        0 0 1 ;];   
% [xp1 xp2]=getpts;
% Hsc2 = homography2d(x1, x2);
Hm = mtimes(Hsc, x3);
outimg2 = imTrans(outimg,Hm);
% 
outimg2 = imresize(outimg2,[2*195 2*259]);
figure(3),imshow(outimg2,[0 255]); 


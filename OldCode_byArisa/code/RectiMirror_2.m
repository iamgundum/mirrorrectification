% for imgnum=1:9
%     
% num=int2str(imgnum);
imgname = 'm7';
%  imgname = ['MIRROR-0' num '2'];
imgname = ['mirror' num];
IpImage = imread(['C:\Users\Lenovo\Desktop\Arisa\mirror image acquisition\cropped\' imgname '.JPG']); 
% IpImage = imread(['C:\Users\s1700564025\Desktop\Arisa\mirror data\' imgname '.JPG']); 
GrImage = rgb2gray(IpImage); GrImage = imadjust(GrImage);
[x y] = size(GrImage);
Iblur1 = imgaussfilt(GrImage,10);
% figure(1), imshow(Iblur1)
imwrite(Iblur1,['C:\Users\Lenovo\Desktop\Arisa\mirror image acquisition\cropped\results' imgname '.JPG'])

[centers, radii, metric] = imfindcircles(Iblur1,[100 250]);

if mean(size(centers)) > 0  %% Circle
    
centers(1) = mean(centers(1)); centers(2) = mean(centers(2));
% Create a logical image of a circle with specified
% diameter, center, and image size.
% First create the image.
imageSizeX = x;
imageSizeY = y;
[columnsInImage rowsInImage] = meshgrid(1:imageSizeX, 1:imageSizeY);
% Next create the circle in the image.
centerX = centers(1); centerY = centers(2);
radius = mean(radii);
circlePixels = (rowsInImage - centerY).^2 + ...
      (columnsInImage - centerX).^2 <= radius.^2;
% circlePixels is a 2D "logical" array.
figure(2),imshow(circlePixels) ;
imwrite(circlePixels,['C:\Users\s1700564025\Desktop\Arisa\mirror data\Mirror results\FG\' imgname '.JPG'])

Foreground = circlePixels;

% %%remove background 
GrImage2 = GrImage; cnt_xy=1; Posit_x=0; Posit_y=0; 

[x y] = size(GrImage2);
for col = 1:x
    for row = 1:y
        if Foreground(row,col)==0
           GrImage2(row,col)=0;
           
        else
            Posit_x(cnt_xy)=col;
            Posit_y(cnt_xy)=row;
            cnt_xy=cnt_xy+1;
        end
    end
end

%%Image correction
Xn=max(Posit_x)-min(Posit_x); Yn=max(Posit_y)-min(Posit_y);
New_GrImg=GrImage2(min(Posit_y):max(Posit_y),min(Posit_x):max(Posit_x));
imwrite(New_GrImg,['C:\Users\s1700564025\Desktop\Arisa\mirror data\Mirror results\MirrorDet\' imgname '.JPG'])
figure(2), imshow(New_GrImg)
%MAPPING
% I2 = lensdistort(New_GrImg, 0.1);
% figure(3), imshow(I2)
% imwrite(I2,['C:\Users\s1700564025\Desktop\Arisa\mirror data\Mirror results\MirrorReconstruct\' imgname '.JPG'])

else
    
GrImage2 = GrImage;


end
% figure(1), imshow(GrImage2)

imwrite(GrImage2,['C:\Users\s1700564025\Desktop\Arisa\mirror data\Mirror results\RemovedBG\' imgname '.JPG'])

%  end
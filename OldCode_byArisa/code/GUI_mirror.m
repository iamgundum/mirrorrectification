function varargout = GUI_mirror(varargin)
% GUI_MIRROR MATLAB code for GUI_mirror.fig
%      GUI_MIRROR, by itself, creates a new GUI_MIRROR or raises the existing
%      singleton*.
%
%      H = GUI_MIRROR returns the handle to a new GUI_MIRROR or the handle to
%      the existing singleton*.
%
%      GUI_MIRROR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_MIRROR.M with the given input arguments.
%
%      GUI_MIRROR('Property','Value',...) creates a new GUI_MIRROR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_mirror_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_mirror_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_mirror

% Last Modified by GUIDE v2.5 17-May-2018 19:57:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_mirror_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_mirror_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

 
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_mirror is made visible.
function GUI_mirror_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_mirror (see VARARGIN)

% Choose default command line output for GUI_mirror
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI_mirror wait for user response (see UIRESUME)
% uiwait(handles.figure1);
set(handles.popupmenu2,'Enable','off') 
set(handles.radiobutton1,'Enable','off') 
set(handles.radiobutton2,'Enable','off') 
set(handles.radiobutton3,'Enable','off') 
set(handles.radiobutton5,'Enable','off') 
set(handles.pushbutton2,'Enable','off') 
set(handles.slider1,'Enable','off') 

% --- Outputs from this function are returned to the command line.
function varargout = GUI_mirror_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2

set(handles.radiobutton1,'Enable','off') 
set(handles.radiobutton2,'Enable','off') 
set(handles.radiobutton3,'Enable','off') 
set(handles.slider1,'Enable','off')

handles = guidata(hObject);  % Update!
 switch get(handles.popupmenu2,'Value')   
  case 1
      S = handles.RGBimage;
      
  case 2
      S = handles.RGBimage;
      
  case 3
      S = handles.RGBimage;
      S = rgb2gray(S);
      
  case 4
      S = handles.RGBimage;
      GrImage = rgb2gray(S); 
      GrImage = imadjust(GrImage);
      [x y] = size(GrImage);
      Iblur1 = imgaussfilt(GrImage,10);
      [centers, radii, metric] = imfindcircles(Iblur1,[100 250]);

        if mean(size(centers)) > 0  %% Circle

        centers(1) = mean(centers(1)); centers(2) = mean(centers(2));
        % Create a logical image of a circle with specified
        % diameter, center, and image size.
        % First create the image.
        imageSizeX = x;
        imageSizeY = y;
        [columnsInImage rowsInImage] = meshgrid(1:imageSizeX, 1:imageSizeY);
        % Next create the circle in the image.
        centerX = centers(1); centerY = centers(2);
        radius = mean(radii);
        circlePixels = (rowsInImage - centerY).^2 + ...
              (columnsInImage - centerX).^2 <= radius.^2;
        % circlePixels is a 2D "logical" array.
        Foreground = circlePixels;

        % %%remove background 
        GrImage2 = GrImage; cnt_xy=1; Posit_x=0; Posit_y=0; 

        [x y] = size(GrImage2);
        for col = 1:x
            for row = 1:y
                if Foreground(row,col)==0
                   GrImage2(row,col)=0;

                else
                    Posit_x(cnt_xy)=col;
                    Posit_y(cnt_xy)=row;
                    cnt_xy=cnt_xy+1;
                end
            end
        end
        Xn=max(Posit_x)-min(Posit_x); Yn=max(Posit_y)-min(Posit_y);
        New_GrImg=GrImage2(min(Posit_y):max(Posit_y),min(Posit_x):max(Posit_x));
        S = New_GrImg;
        handles.ForegroundImg = S;
        else
            S = zeros(size(S));
            handles.ForegroundImg = S;
        end
              
   case 5
      set(handles.radiobutton1,'Enable','on') 
      set(handles.radiobutton2,'Enable','on') 
      set(handles.radiobutton3,'Enable','on')
      set(handles.slider1,'Enable','on')
      if sum(handles.ForegroundImg) == 0
         S = zeros(size(handles.ForegroundImg));
         handles.DistorImg = S; 
      end
        S = handles.ForegroundImg;
        handles.DistorImg = S; 
  
  end  
handles.S = S;
guidata(hObject, handles);  % Update!
set(handles.pushbutton2,'Enable','on') 


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushopen.
function pushopen_Callback(hObject, eventdata, handles)
% hObject    handle to pushopen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton2,'Enable','on') 
set(handles.radiobutton5,'Enable','on') 
set(handles.radiobutton1,'Enable','off') 
set(handles.radiobutton3,'Enable','off')

[F,PathName,FilterIndex] = uigetfile({'*.*','All Files(*.*)'}, 'Select your File ');
input=strcat(PathName,F);
RGBimage=importdata(input); % ?????????????????????? RGBimage
axes(handles.axes2); % ???????? axes ??????????????????????
imshow(RGBimage); % ???????????????????????????? MATLAB ????
hpixinfo = impixelinfo; % ??? ??????????????????? ???????? R, G ??? B ????? pixel ?????
zoom on;
axis off;
handles.input = input;
handles.RGBimage = RGBimage;
guidata(hObject,handles);
set(handles.popupmenu2,'Enable','on') 

handles.Snake = 0;

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
   
imshow(handles.S)




% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2


% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
 
a = get(handles.slider1, 'Value');
handles.a = a;

set(handles.text7,'String',num2str(handles.a))

% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes2


% --- Executes on key press with focus on popupmenu2 and none of its controls.
function popupmenu2_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when selected object is changed in uibuttongroup1.
function uibuttongroup1_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uibuttongroup1 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function uibuttongroup1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uibuttongroup1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function text7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton3.
function handles=pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.I = rgb2gray(handles.RGBimage)  ;
[x y] = size(handles.I);
handles.I = imresize(handles.I,[x/4 y/4])
imshow(handles.I)
handles.I = im2double(handles.I)  ;
set(handles.slider1,'Enable','on','Value',0.2) 
set(handles.text7,'String',0.2)
set(handles.text6,'String','Distant')
% Get coordinate from GUI 
[AutoSegVal.SeedPty, AutoSegVal.SeedPtx] = getpts;
% region growing 
%  initPos = [round(AutoSegVal.SeedPtx),round(AutoSegVal.SeedPty)];
% [P, SegmentedMask] = regionGrowing(inputImg, initPos, AutoSegVal.SlideBarVal, 120, 0, 1, 0);
AutoSegVal.SlideBarVal=get(handles.slider1, 'Value');
if isfield(AutoSegVal,'SlideBarVal') == 0
    AutoSegVal.SlideBarVal = 0.2;
end
SegmentedMask = regiongrowing(handles.I,round(AutoSegVal.SeedPtx),round(AutoSegVal.SeedPty),AutoSegVal.SlideBarVal);
SegmentedMask = imfill(SegmentedMask,'holes');
[x y] = size(SegmentedMask);
N=zeros(size(handles.I));
for i = 1:x
    for j = 1:y
        if SegmentedMask(i,j) == 1
            N(i,j) = handles.I(i,j);
        end
    end
end
imshow(N)
handles.S=N;
% imshow(handles.I+SegmentedMask)



% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% inimg = rgb2gray(handles.RGBimage)  ;
set(handles.radiobutton2,'Enable','on') 
set(handles.radiobutton5,'Enable','on') 
set(handles.radiobutton1,'Enable','off') 
set(handles.radiobutton3,'Enable','off')

if  get(handles.radiobutton2,'Value') ==1
%no mirror
inimg = getimage(handles.axes2);
[w h]=size(inimg);
inimg = imresize(inimg,[2*195 2*259]);
axes(handles.axes2);
cla;
imshow(inimg)

[xp1 xp2]=getpts;
x1 = [xp1(1) xp1(2) xp1(3) xp1(4);
      xp2(1) xp2(2) xp2(3) xp2(4);
        1   1   1   1;];   
% x2: desired points for the output plane
x2 = [ 20 450 450  20;
       20  20 320 320;
        1   1   1   1;];
Hsc = homography2d(x1, x2);
outimg = imTrans(inimg,Hsc);
% display the result
% subplot(2,1,1); 
figure(2)
imshow(inimg);hold on
plot([xp1(1),xp1(2)],[xp2(1),xp2(2)],'g')
plot([xp1(3),xp1(2)],[xp2(3),xp2(2)],'g')
plot([xp1(1),xp1(4)],[xp2(1),xp2(4)],'g')
plot([xp1(4),xp1(3)],[xp2(4),xp2(3)],'g')
handles.outimg = outimg;
handles.Hsc = Hsc;

elseif get(handles.radiobutton5,'Value') ==1
%flat mirror
inimg = getimage(handles.axes2);
[w h]=size(inimg);
inimg = imresize(inimg,[2*195 2*259]);
axes(handles.axes2);
cla;
imshow(inimg)

[xp1 xp2]=getpts;
x1 = [xp1(1) xp1(2) xp1(3) xp1(4);
      xp2(1) xp2(2) xp2(3) xp2(4);
        1   1   1   1;];   
% x2: desired points for the output plane
x2 = [ 20 450 450  20;
       20  20 320 320;
        1   1   1   1;];
Hsc = homography2d(x1, x2);
outimg = imTrans(inimg,Hsc);
% display the result
% subplot(2,1,1); 
figure(2)
imshow(inimg);hold on
plot([xp1(1),xp1(2)],[xp2(1),xp2(2)],'g')
plot([xp1(3),xp1(2)],[xp2(3),xp2(2)],'g')
plot([xp1(1),xp1(4)],[xp2(1),xp2(4)],'g')
plot([xp1(4),xp1(3)],[xp2(4),xp2(3)],'g')
handles.outimg = outimg;
handles.Hsc = Hsc;    

inimg = getimage(handles.axes2);
%reflect about y-axis
x3 = [-1 0 0;
       0 1 0 ;
       0 0 1 ;];
%%reflect about x-axis
% x3 = [1 0 0;
%        0 -1 0 ;
%        0 0 1 ;];   

%%reflect about origin
% x3 = [-1 0 0;
%        0 -1 0 ;
%        0 0 1 ;];   
   
Hsc = handles.Hsc;
Hm = mtimes(Hsc, x3);
outimg = imTrans(inimg,Hm);
handles.outimg = outimg;

elseif get(handles.radiobutton3,'Value') ==1
%Barrel
inimg = getimage(handles.axes2);
[w h]=size(inimg);
inimg = imresize(inimg,[2*195 2*259]);
axes(handles.axes2);
cla;
imshow(inimg)



elseif get(handles.radiobutton1,'Value') ==1
%Pincushion
inimg = getimage(handles.axes2);
[w h]=size(inimg);
inimg = imresize(inimg,[2*195 2*259]);
axes(handles.axes2);
cla;
imshow(inimg)

end
axes(handles.axes2);
% figure(2)
% subplot(2,1,2);
imshow(handles.outimg,[0 255]); 
outimg=imcrop(outimg);
axes(handles.axes2);
imshow(outimg,[0 255]); 
guidata(hObject,handles) ;


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function text6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes2);
img=getimage(handles.axes2);
img=uint8(img);
[FileName, PathName] = uiputfile('*.bmp', 'Save As');
if PathName==0, return; end 
Name = fullfile(PathName,FileName);  %# <--- reverse the order of arguments
imwrite(img, Name, 'bmp');


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobutton1,'Enable','on') 
set(handles.radiobutton2,'Enable','off') 
set(handles.radiobutton3,'Enable','on')
I=getimage(handles.axes2);
handles.I2 =I;
if  get(handles.radiobutton1,'Value') ==1
    %Pincushion
    set(handles.slider1,'Enable','on','Value',0.3) 
    set(handles.text7,'String',0.3)
    set(handles.text6,'String','Distortion')
    AutoSegVal.SlideBarVal=get(handles.slider1, 'Value');
    handles.I2 = lensdistort(I, -(AutoSegVal.SlideBarVal))  
    
elseif get(handles.radiobutton3,'Value') ==1
    %Barrel
    set(handles.slider1,'Enable','on','Value',0.3) 
    set(handles.text7,'String',0.3)
    set(handles.text6,'String','Distortion')
    AutoSegVal.SlideBarVal=get(handles.slider1, 'Value');
    handles.I2 = lensdistort(I, AutoSegVal.SlideBarVal)
end
axes(handles.axes2);
imshow(handles.I2)


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes2);
imshow(handles.input)


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton5

function[feature_img, std_img] = specular_detection(img,SHOW_RESULTS,saveindex)
%% img convert
hsv_img = rgb2hsv(img);
lab_img = rgb2lab(img);

%% SR Detection
g_img = img(:,:,2);
g_img = g_img/255;

s_img = hsv_img(:,:,2);

l_img = lab_img(:,:,1);
l_img = l_img/100;

feature_img = (1-s_img).*(double(g_img).*l_img);
feature_img = feature_img.*feature_img.*feature_img;

%% Standard deviation
std_img = stdfilt(feature_img);
std_img = std_img/max(max(std_img));
%% Show Results
if SHOW_RESULTS == 1
    
%     save_fig = figure;
%     subplot(1,3,1)
%     imshow(img);
%     title('RGB')
% 
%     subplot(1,3,2)
%     imshow(feature_img);
%     title('Feature')
%     
%     subplot(1,3,3)
%     imshow(std_img);
%     title('STD')
    
    std_imgzerofive = std_img>0.5;

    save_fig = figure;
    subplot(1,3,1)
    imshow(img);
    title('RGB')

    subplot(1,3,2)
    imshow(std_img);
    title('STD')
    
    subplot(1,3,3)
    imshow(std_imgzerofive);
    title('STD0.5')

    if saveindex ~= 0
%         saveas(save_fig,['./specular_results/show/' num2str(saveindex) '.png']);
%         imwrite(feature_img,['./specular_results/features/' num2str(saveindex) '.png'  ]);
%         imwrite(std_img,['./specular_results/std/' num2str(saveindex) '.png'  ]);

%         imwrite(std_imgzerofive,['./specular_results/std0.5/' num2str(saveindex) '.png'  ]);
        saveas(save_fig,['./specular_results/show2/' num2str(saveindex) '.png']);
    end
    
    close all
end

end
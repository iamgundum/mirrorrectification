clear
close all
%%input
db_Dir = ['../mirror data'];
db_Images = imageSet(db_Dir);
numImages = numel(db_Images.ImageLocation);

for i=1:numImages
    img = read(db_Images, i);
    [feature_img, std_img] = specular_detection(img,1,i); %1 for subplot a results, i for save
%     imshow(img);
end

